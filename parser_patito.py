from typing import Dict, List, Tuple


class Parser:
    def __init__(self, file_path: str):
        with open(file_path, 'r') as f:
            self.lines = f.readlines()

    def parse(self, scale_factor=1) -> Tuple[List, Dict[str, List]]:

        mtl_indexes = []

        vertex_raw = [line for line in self.lines if 'v ' in line]
        vertex_3_array = [list(map(lambda e: float(e), v_line.replace('\n', '').strip().split(' ')[1:])) for v_line in
                          vertex_raw]

        vertex_array = []
        for vertex in vertex_3_array:
            vertex_array.append(vertex[0] * scale_factor)
            vertex_array.append(vertex[1] * scale_factor)
            vertex_array.append(vertex[2] * scale_factor)

        segments = {}
        for i, line in enumerate(self.lines):
            if 'usemtl ' in line:
                mtl_indexes.append(i)
        mtl_indexes.append(len(self.lines))

        for i in range(len(mtl_indexes) - 1):
            material_name = f'{self.lines[mtl_indexes[i]].strip().split()[-1]}_{i}'

            raw_lines = self.lines[mtl_indexes[i]: mtl_indexes[i + 1]]

            index_raw = [line for line in raw_lines if 'f ' in line]
            index_tuple_array = [
                list(map(lambda e: int(e.split('/')[0]) - 1, i_line.replace('\n', '').strip().split(' ')[1:])) for
                i_line in index_raw]

            index_array = []
            for index_tuple in index_tuple_array:

                # Lo volvemos triangulo
                for i in range(len(index_tuple) - 2):
                    index_array.append(index_tuple[0])
                    index_array.append(index_tuple[i + 1])
                    index_array.append(index_tuple[i + 2])

            segments[material_name] = index_array

        return vertex_array, segments

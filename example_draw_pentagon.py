from OpenGL.GL import *
from OpenGL.GLUT import *

from ctypes import c_float, c_short, c_char

from Quaternion import *

window = 0
width, height = 800, 830  # window size

mouse_x, mouse_y = 0, 0
bounds = (-4, 4, -4, 4)

x_angle = 0
y_angle = 0
z_angle = 0

h_state = 0
v_state = 0

h_increment = 0
v_increment = 0

clicked = False

q = np.array([0, 0, 0, 1])
quaternion_matrix = toMatrix(np.array([0, 0, 0, 1]))
normals = np.array([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]])


def refresh2d(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    if width <= height:
        glOrtho(-2, 2, -2 * height/width, 2 * height/width, -10, 10)
    else:
        glOrtho(-2 * width/height, 2 * width/height, -2, 2, -10, 10)

    # glOrtho(-10, 10, -10, 10, -10, 10)
    # glOrtho(-5, 5, -5, 5, -5, 5)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glShadeModel(GL_FLAT)
    glEnable(GL_DEPTH_TEST)


def draw():  # ondraw is called all the time
    global x_angle, y_angle, z_angle

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # clear the screen
    glLoadIdentity()  # reset position

    refresh2d(width, height)

    if h_increment != 0:
        rotate(h_increment, [0, 1, 0])
    if v_increment != 0:
        rotate(v_increment, [1, 0, 0])

    glMultMatrixf(quaternion_matrix)

    v = [-.5, -.5, 0,
         0.5, -.5, 0,
         0.7, .25, 0,
         0., 0.75, 0,
         -.7, .25, 0]
    vertex_c = (c_float * len(v))(*v)

    indices = [0, 1, 2,
               0, 2, 3,
               0, 3, 4]
    index_c = (c_short * len(indices))(*indices)

    colors = [255, 0, 0,    #
              255, 255, 0,  # LOS PRIMEROS TRES VERTICEs
              255, 0, 0,    #
              255, 255, 0,    # A PARTIR DE ACA EL ULTIMO VERTICE CAMBIA EL TRIANGULO
              255, 255, 255]  # LO MISMO CAMBIA EL TRIANGULO ENTERO
    colors_c = (c_char * len(colors))(*colors)

    glEnableClientState(GL_VERTEX_ARRAY)
    glEnableClientState(GL_COLOR_ARRAY)

    n_dims = 3
    glVertexPointer(n_dims, GL_FLOAT, 0, vertex_c)

    n_componens = 3
    glColorPointer(n_componens, GL_UNSIGNED_BYTE, 0, colors_c)

    glDrawElements(GL_TRIANGLES, len(index_c), GL_UNSIGNED_SHORT, index_c)

    glDisableClientState(GL_VERTEX_ARRAY)
    glDisableClientState(GL_COLOR_ARRAY)

    # vertex = [(-.5, -.5, 0),
    #           (0.5, -.5, 0),
    #           (0.7, .25, 0),
    #           (0., 0.75, 0),
    #           (-.7, .25, 0)]
    #
    # glBegin(GL_LINE_LOOP)
    # for v in vertex:
    #     glVertex3f(*v)
    # glEnd()

    glFlush()

    x_angle = (x_angle + v_increment) % 360
    y_angle = (y_angle + h_increment) % 360
    z_angle = 0


def hold_movement(mx, my):
    global clicked, h_state, v_state, h_increment, v_increment
    global cube

    if clicked:
        h_increment = (mx - h_state)*0.07
        h_state = mx

        v_increment = (my - v_state)*0.07
        v_state = my
    else:
        h_increment = 0
        v_increment = 0


def click_control(button, state, x, y):
    global clicked, h_state, v_state, h_increment, v_increment

    if state == GLUT_DOWN and button == GLUT_LEFT_BUTTON:
        clicked = (clicked + 1) % 2
        h_state = x
        v_state = y

        if not clicked:
            h_increment = 0
            v_increment = 0

    if state == GLUT_DOWN and button == GLUT_RIGHT_BUTTON:
        cube.state = (cube.state + 1)%3


def rotate(degrees, axis):
    global quaternion_matrix, normals, q

    r = quaternion(np.array(axis), degrees)
    normals[0] = encase3(normals[0], r)
    normals[1] = encase3(normals[1], r)
    normals[2] = encase3(normals[2], r)
    q = quatMult(q, quaternion(axis, -degrees))
    quaternion_matrix = toMatrix(q)


if __name__ == '__main__':

    glutInit()  # initialize glut
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(width, height)
    glutInitWindowPosition(0, 0)
    window = glutCreateWindow("Rect")
    glutDisplayFunc(draw)
    glutIdleFunc(draw)

    glutMouseFunc(click_control)

    glutPassiveMotionFunc(hold_movement)

    glutMainLoop()

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.raw.GLU import gluLookAt

from Quaternion import *

from test_object2 import ObjectFromFile, Street

window = 0
# width, height = 800, 830  # window size
width, height = 1900, 1080  # window size

mouse_x, mouse_y = 0, 0
bounds = (-4, 4, -4, 4)

x_angle = 0
y_angle = 0
z_angle = 0

# Rotation States
clicked_left = False
h_state = 0
v_state = 0
h_increment = 0
v_increment = 0

# Movement States
clicked_right = False
h_mov_state = 0
v_mov_state = 0
h_mov_increment = 0
v_mov_increment = 0

h_mov_dist = 0
v_mov_dist = 0
z_mov_dist = 0
slow_factor = 1

q = None
quaternion_matrix = None
normals = None

quaternion_matrix_bak = []
q_bak = []
normals_bak = []


def load_identity_quaternion():
    global quaternion_matrix, q, normals

    q = np.array([0, 0, 0, 1])
    quaternion_matrix = toMatrix(np.array([0, 0, 0, 1]))
    normals = np.array([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]])


def push_quaternion():
    global quaternion_matrix, q, normals
    global quaternion_matrix_bak, q_bak, normals_bak

    glPushMatrix()
    quaternion_matrix_bak.append(quaternion_matrix.copy())
    q_bak.append(q.copy())
    normals_bak.append(normals.copy())


def pop_quaternion():
    global quaternion_matrix, q, normals
    global quaternion_matrix_bak, q_bak, normals_bak

    glPopMatrix()
    quaternion_matrix = quaternion_matrix_bak.pop()
    q = q_bak.pop()
    normals = normals_bak.pop()


def refresh2d(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    limit = 6 + z_mov_dist
    if width <= height:
        glOrtho(-limit, limit, -limit * height / width, limit * height / width, -10, 10)
    else:
        glOrtho(-limit * width / height, limit * width / height, -limit, limit, -10, 10)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glShadeModel(GL_FLAT)
    glEnable(GL_DEPTH_TEST)

    # glViewport(0, 0, width, height)
    # gluLookAt(0, 0, 0,
    #           0, 0, -1,
    #           0, 1, 0)


def draw_translate(drawable, translate_vector):
    glPushMatrix()
    glTranslate(*translate_vector)
    drawable.draw()
    glPopMatrix()


def draw():  # ondraw is called all the time
    global x_angle, y_angle, z_angle
    global h_mov_dist, v_mov_dist

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # clear the screen
    glLoadIdentity()  # reset position

    refresh2d(width, height)

    # Mouse Rotate
    if h_increment != 0:
        rotate(h_increment, [0, 1, 0])
    if v_increment != 0:
        rotate(v_increment, [1, 0, 0])
    glMultMatrixf(quaternion_matrix)

    """Drawing"""

    glTranslate(0, 0, 4)

    glTranslate(0, h_mov_dist, 0)
    glTranslate(v_mov_dist, 0, 0)
    # glTranslate(0, 0, z_mov_dist)

    # Street
    draw_translate(street, (0, 0, 1))
    draw_translate(street, (0, 0, -4.701))
    draw_translate(street, (0, 0, -7.701))

    # Pine
    draw_translate(simple_tree, (-3, 0.93, 0))
    draw_translate(simple_tree, (-3, 0.93, -5))
    draw_translate(simple_tree, (3, 0.93, 3))
    draw_translate(simple_tree, (3, 0.93, -3))
    draw_translate(simple_tree, (3, 0.93, -9))
    draw_translate(simple_tree, (-2, 0.93, -7.5))

    # Wolf
    glPushMatrix()
    draw_translate(wolf, (-2, 0, 0))
    glPopMatrix()

    # Tree
    draw_translate(special_tree, (-4, 0.1, 3.5))
    draw_translate(special_tree, (3, 0.1, 1))
    draw_translate(special_tree, (-3, 0.1, -3))
    draw_translate(special_tree, (-3.5, 0.1, -9))

    # Rock
    draw_translate(rocks, (2, 0, 0))
    glPushMatrix()
    glRotatef(290, 0, 1, 0)
    draw_translate(rocks, (-7.5, 0, -7))
    glPopMatrix()

    # Grass
    for x, z in zip(grass_positions_x, grass_positions_z):
        draw_translate(grass, (x, 0, z))
    draw_translate(grass, (-3, 0, -9))
    draw_translate(grass, (-3, 0, -8))
    draw_translate(grass, (-4, 0, -8.5))

    # House
    glPushMatrix()
    glRotate(270, 0, 1, 0)
    draw_translate(house_1, (-7.5, -0.6, 2))
    glPopMatrix()

    # Car
    glPushMatrix()
    glRotatef(90, 0, 1, 0)
    draw_translate(car, (7, 0.125, 0.7))
    glPopMatrix()

    glFlush()

    x_angle = (x_angle + v_increment) % 360
    y_angle = (y_angle + h_increment) % 360
    z_angle = 0

    h_mov_dist += h_mov_increment
    if h_mov_dist > 6.225: h_mov_dist = 6.225
    if h_mov_dist < -6.225: h_mov_dist = -6.225

    v_mov_dist += v_mov_increment
    if v_mov_dist > 6: v_mov_dist = 6
    if v_mov_dist < -6: v_mov_dist = -6


def hold_movement(mx, my):
    global clicked_left, clicked_right

    # speed = 0.07
    speed = 1

    if clicked_left:
        global h_state, v_state, h_increment, v_increment

        h_increment = (mx - h_state) * (speed * slow_factor)
        h_state = mx

        v_increment = (my - v_state) * (speed * slow_factor)
        v_state = my
    else:
        h_increment = 0
        v_increment = 0

    if clicked_right:
        global h_mov_state, v_mov_state, h_mov_increment, v_mov_increment
        h_mov_increment = (-my + h_mov_state) * (0.01 * (slow_factor**2))
        print(h_mov_increment)
        h_mov_state = my

        v_mov_increment = (mx - v_mov_state) * (0.01 * slow_factor**2)
        v_mov_state = mx
    else:
        h_mov_increment = 0
        v_mov_increment = 0


def click_control(button, state, x, y):
    if state == GLUT_DOWN and button == GLUT_LEFT_BUTTON:
        global clicked_left, h_state, v_state, h_increment, v_increment
        clicked_left = (clicked_left + 1) % 2
        h_state = x
        v_state = y

        if not clicked_left:
            h_increment = 0
            v_increment = 0

    if state == GLUT_DOWN and button == GLUT_RIGHT_BUTTON:
        global clicked_right, h_mov_state, v_mov_state, h_mov_increment, v_mov_increment
        global h_mov_dist, v_mov_dist

        clicked_right = (clicked_right + 1) % 2
        h_mov_state = y
        v_mov_state = x

        if not clicked_right:
            h_mov_increment = 0
            v_mov_increment = 0

    global z_mov_dist, slow_factor
    if button == 3:
        # Wheel Up
        z_mov_dist += 0.1
        slow_factor *= 1.1
        if slow_factor > 1: slow_factor = 1
    if button == 4:
        # Wheel Down
        z_mov_dist -= 0.1
        slow_factor *= 0.99
        if slow_factor == 0.000000001: slow_factor = 0.1


def wheel_action(wheel, direction, x, y):
    print(wheel, direction, x, y)


def rotate(degrees, axis):
    global quaternion_matrix, normals, q

    r = quaternion(np.array(axis), degrees)
    normals[0] = encase3(normals[0], r)
    normals[1] = encase3(normals[1], r)
    normals[2] = encase3(normals[2], r)
    q = quatMult(q, quaternion(axis, -degrees))
    quaternion_matrix = toMatrix(q)


if __name__ == '__main__':

    load_identity_quaternion()

    car = ObjectFromFile('loj0agpflbsw-car/car.obj',
                         scale_factor=0.25,
                         materials_list=[
                             'Material.010_6', 'Material.011_7', 'Material.012_8', 'Material.010_9', 'Material.011_10',
                             'Material.012_11', 'Material.010_12', 'Material.011_13', 'Material.012_14',
                             'Material.010_18', 'Material.011_19',
                             'Material.012_20', 'Material.004_21', 'Material.005_22', 'Material.006_23',
                             'Material.007_24', 'Material.008_25', 'Material.009_26', 'Material.017_27'
                         ],
                         colors_dict={
                             'aro_ex_fd': [183, 183, 183],
                             'llanta_fd': [36, 36, 36],
                             'aro_in_fd': [159, 159, 159],
                             'aro_ex_td': [183, 183, 183],
                             'llanta_td': [36, 36, 36],
                             'aro_in_td': [159, 159, 159],
                             'aro_ex_ti': [183, 183, 183],
                             'llanta_ti': [36, 36, 36],
                             'aro_in_ti': [159, 159, 159],
                             'aro_ex_di': [183, 183, 183],
                             'llanta_di': [36, 36, 36],
                             'aro_in_di': [159, 159, 159],
                             'chasis': [0, 255, 221],
                             'resto': [51, 51, 51]
                         })

    # grass_positions_z = np.round(np.random.uniform(-7, 4, 30), 2)
    # grass_positions_x = np.round(np.random.uniform(-5, 3, 30), 2)

    grass_positions_x = [-3.79, -1.07-2, 2.1, 1.31+1, 2.03-1, 0.28+3, -0.31-2, -0.84-3, 0.48, -2.13-1, -2.91, -0.71-4,
                         -3.93, 1.22, -0.57+3, 1.85, -0.66-3, -3.0, -0.96+3, -0.61+3, 2.21, -3.55, -4.75, 1.33,
                         -1.69-2, -3.58, 1.06, 0.61, 1.8, -4.27]
    grass_positions_z = [2.0, -5.03, 1.29, 3.34, -2.88, -5.49, -6.61, -5.28, -2.94, -0.33, -3.37, 2.39, -3.11, -1.83,
                         1.47, 3.75, -6.07, -2.85, 0.87, -0.34, -3.74, -0.04, 1.19, 1.57, -0.39, 3.0, -3.29, -6.0,
                         -6.79, 1.55]

    simple_tree = ObjectFromFile('Tree/lowpolytree.obj',
                                 scale_factor=0.5,
                                 colors_dict={'leaves': [78, 140, 31],
                                              'bark': [102, 59, 25]})

    street = Street()
    wolf = ObjectFromFile('nfts8bvk6s5c-Wolf/Wolf.obj',
                          scale_factor=0.003,
                          colors_dict={'wolf': [135, 142, 150]})

    rocks = ObjectFromFile('BlenderNatureAsset/BlenderNatureAsset.obj',
                           materials_list=[
                               'Material.004_17', 'Material.005_25', 'Material.006_26'
                           ],
                           scale_factor=0.4,
                           colors_dict={
                               'rock_1': [71, 81, 83],
                               'rock_2': [71, 81, 83],
                               'rock_3': [71, 81, 83]
                           })

    grass_color = [34, 136, 62]
    grass = ObjectFromFile('BlenderNatureAsset/BlenderNatureAsset.obj',
                           materials_list=[
                               'Material.007_0', 'Material.007_1', 'Material.007_2', 'Material.007_3', 'Material.007_4'
                           ],
                           scale_factor=0.4,
                           colors_dict={
                               'grass_1': grass_color,
                               'grass_2': grass_color,
                               'grass_3': grass_color,
                               'grass_4': grass_color,
                               'grass_5': grass_color
                           })

    special_tree = ObjectFromFile('BlenderNatureAsset/BlenderNatureAsset.obj',
                                  materials_list=[
                                      'troc_21', 'Material.001_22', 'Material.002_23', 'Material.003_24'
                                  ],
                                  scale_factor=0.7,
                                  colors_dict={
                                      'bark': [102, 59, 25],
                                      'leaves_1': [78, 140, 31],
                                      'leaves_2': [78, 140, 31],
                                      'leaves_3': [78, 140, 31],
                                  })

    # assets = ObjectFromFile('12-asset-pack.obj/Asset Pack.obj',
    #                         scale_factor=0.5,
    #                         materials_list=[
    #                             'Material.068_0', 'Material.070_1', 'Material.071_2', 'Material.072_3',
    #                             'Material.067_4', 'Material.012_5', 'Material.012_6', 'Material.066_7', 'Stone_8',
    #                             'metal_9', 'wood_10', 'Stone_11', 'Stone_12', 'Stone_13', 'Material.053_14',
    #                             'Material.054_15', 'Material.057_16', 'Material.063_17', 'Material.055_18',
    #                             'Material.056_19', 'Material.059_20', 'Material.060_21', 'Material.061_22',
    #                             'Material.062_23', 'Material.045_24', 'Material.044_25', 'Stone.001_26', 'Stone2_27',
    #                             'Material.052_28', 'Material.038_29', 'Material.042_30', 'Material.037_31',
    #                             'Material.039_32', 'Material.035_33', 'Material.043_34', 'Material.023_35',
    #                             'Material.024_36', 'Tree_Fir_Wood_37', 'Material.022_38', 'Tree_Fir_Wood_39',
    #                             'Material.022_40', 'Material.020_41', 'Material.033_42', 'Material.025_43',
    #                             'Material.026_44', 'Material.014_45', 'Material.031_46', 'Material.030_47',
    #                             'Material.029_48', 'Material_49', 'Material.019_50', 'Material.018_51',
    #                             'Material.017_52', 'Material.013_53', 'Material.025_54', 'Material.026_55',
    #                             'Material.014_56', 'Material.031_57', 'Material.030_58', 'Material.029_59',
    #                             'Material_60', 'Material.019_61', 'Material.018_62', 'Material.017_63',
    #                             'Material.013_64', 'Stone_65', 'Stone_66', 'Tree_Fir_Wood_67', 'Material.022_68',
    #                             'Material.011_71', 'Material.004_72', 'Material.005_73',
    #                             'Material.009_74', 'Material.008_75', 'Material.006_76', 'Material.010_77',
    #                             'Material.007_78', 'Material.016_79', 'Material.015_80', 'Material.001_81',
    #                             'Material.000_82', 'Material.021_83', 'Material.003_84', 'Material.002_85',
    #                             'Material.002_86', 'Material.032_87'
    #                         ])

    house_1 = ObjectFromFile('12-asset-pack.obj/Asset Pack.obj',
                             scale_factor=1.2,
                             materials_list=[
                                'Material.025_54', 'Material.026_55',
                                'Material.014_56', 'Material.031_57', 'Material.030_58', 'Material.029_59',
                                'Material_60', 'Material.019_61', 'Material.018_62', 'Material.017_63',
                                'Material.013_64'
                             ],
                             colors_dict={
                                 'marco_ventana': [60, 41, 35],
                                 'vidrio_ventana': [133, 167, 195],
                                 'maderas': [127, 72, 41],
                                 'pomo': [60, 41, 35],
                                 'puerta': [59, 30, 26],
                                 'madera_central': [127, 72, 41],
                                 'techo': [127, 72, 41],
                                 '8': [250, 250, 250],
                                 '9': [209, 209, 209],
                                 '10': [150, 150, 150],
                                 '11': [250, 250, 250]
                             })

    # house_2 = ObjectFromFile('12-asset-pack.obj/Asset Pack.obj',
    #                          scale_factor=0.5,
    #                          materials_list=[
    #                              'Material.020_41', 'Material.033_42', 'Material.025_43',
    #                              'Material.026_44', 'Material.014_45', 'Material.031_46', 'Material.030_47',
    #                              'Material.029_48', 'Material_49', 'Material.019_50', 'Material.018_51',
    #                              'Material.017_52', 'Material.013_53'
    #                          ])

    glutInit()  # initialize glut
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(width, height)
    glutInitWindowPosition(0, 0)
    window = glutCreateWindow("Rect")
    glutDisplayFunc(draw)
    glutIdleFunc(draw)

    glutMouseFunc(click_control)

    glutPassiveMotionFunc(hold_movement)

    glutMouseWheelFunc(wheel_action)

    glutMainLoop()

from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GLU import gluLookAt
import glm
from OpenGL.GL import *

from test_object2 import ObjectFromFile, Street

window = 0
# width, height = 800, 830  # window size
width, height = 1900, 1080  # window size

input_vec = glm.vec4(0, 0, -1, 1)
position = [0, 0, 0]
direction = [0] * 4

rotY = 0.0
rotX = 0.0

mat = glm.mat4x4()

rot_y = 0.0
ant_x = -1


def draw_translate(drawable, translate_vector):
    glPushMatrix()
    glTranslate(*translate_vector)
    drawable.draw()
    glPopMatrix()


def refresh2d(width, height):
    glViewport(0, 0, width, height)
    gluLookAt(0, 0, 0, 0, 0, -1, 0, 1, 0)

    glShadeModel(GL_FLAT)
    glEnable(GL_DEPTH_TEST)


def draw():
    global ant_x
    # glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # clear the screen
    # glLoadIdentity()  # reset position
    #
    # refresh2d(width, height)

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    params = [176 / 255, 196 / 255, 222 / 256, 0]
    glClearColor(*params)

    refresh2d(width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(67, width / height, 1, 100)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glRotatef(rot_y, 0, 1, 0)
    glTranslatef(-position[0], -position[1], -position[2])

    """Objects"""

    glTranslate(0, -0.68, 4)

    # Street
    draw_translate(street, (0, 0, 1))
    draw_translate(street, (0, 0, -4.701))
    draw_translate(street, (0, 0, -7.701))

    # Pine
    draw_translate(simple_tree, (-3, 0.93, 0))
    draw_translate(simple_tree, (-3, 0.93, -5))
    draw_translate(simple_tree, (3, 0.93, 3))
    draw_translate(simple_tree, (3, 0.93, -3))
    draw_translate(simple_tree, (3, 0.93, -9))
    draw_translate(simple_tree, (-2, 0.93, -7.5))

    # Wolf
    glPushMatrix()
    draw_translate(wolf, (-2, 0, 0))
    glPopMatrix()

    # Tree
    draw_translate(special_tree, (-4, 0.1, 3.5))
    draw_translate(special_tree, (3, 0.1, 1))
    draw_translate(special_tree, (-3, 0.1, -3))
    draw_translate(special_tree, (-3.5, 0.1, -9))

    # Rock
    draw_translate(rocks, (2, 0, 0))
    glPushMatrix()
    glRotatef(290, 0, 1, 0)
    draw_translate(rocks, (-7.5, 0, -7))
    glPopMatrix()

    # Grass
    for x, z in zip(grass_positions_x, grass_positions_z):
        draw_translate(grass, (x, 0, z))
    draw_translate(grass, (-3, 0, -9))
    draw_translate(grass, (-3, 0, -8))
    draw_translate(grass, (-4, 0, -8.5))

    # House
    glPushMatrix()
    glRotate(270, 0, 1, 0)
    draw_translate(house_1, (-7.5, -0.6, 2))
    glPopMatrix()

    # Car
    glPushMatrix()
    glRotatef(90, 0, 1, 0)
    draw_translate(car, (7, 0.125, 0.7))
    glPopMatrix()

    glFlush()


def hold_movement(pos_x, pos_y):
    global ant_x, rot_y

    if ant_x == -1:
        ant_x = pos_x
    else:
        rot_y = rot_y + (pos_x - ant_x) / 3
        ant_x = pos_x


def key_function(key, pos_x, pos_y):
    global mat, direction, position

    if key == b'\x1b':
        # Exit on Esc key
        glutLeaveMainLoop()
    if key == b'w':
        mat = glm.mat4x4()
        mat = glm.rotate(mat, glm.radians(-rot_y), (0, 1, 0))
        direction = mat * input_vec

        position[0] = position[0] + direction[0] * 0.1
        position[1] = position[1] + direction[1] * 0.1
        position[2] = position[2] + direction[2] * 0.1
    if key == b's':
        mat = glm.mat4x4()
        mat = glm.rotate(mat, glm.radians(-rot_y), (0, 1, 0))
        direction = mat * input_vec

        position[0] = position[0] - direction[0] * 0.1
        position[1] = position[1] - direction[1] * 0.1
        position[2] = position[2] - direction[2] * 0.1
    if key == b'a':
        mat = glm.mat4x4()
        mat = glm.rotate(mat, glm.radians(90), (0, 1, 0))
        mat = glm.rotate(mat, glm.radians(-rot_y), (0, 1, 0))
        direction = mat * input_vec

        position[0] = position[0] + direction[0] * 0.1
        position[1] = position[1] + direction[1] * 0.1
        position[2] = position[2] + direction[2] * 0.1
    if key == b'd':
        mat = glm.mat4x4()
        mat = glm.rotate(mat, glm.radians(90), (0, 1, 0))
        mat = glm.rotate(mat, glm.radians(-rot_y), (0, 1, 0))
        direction = mat * input_vec

        position[0] = position[0] - direction[0] * 0.1
        position[1] = position[1] - direction[1] * 0.1
        position[2] = position[2] - direction[2] * 0.1


def check_bound(state):
    if state == GLUT_LEFT:
        glutWarpPointer(width // 2, height // 2)


if __name__ == '__main__':

    car = ObjectFromFile('loj0agpflbsw-car/car.obj',
                         scale_factor=0.25,
                         materials_list=[
                             'Material.010_6', 'Material.011_7', 'Material.012_8', 'Material.010_9', 'Material.011_10',
                             'Material.012_11', 'Material.010_12', 'Material.011_13', 'Material.012_14',
                             'Material.010_18', 'Material.011_19',
                             'Material.012_20', 'Material.004_21', 'Material.005_22', 'Material.006_23',
                             'Material.007_24', 'Material.008_25', 'Material.009_26', 'Material.017_27'
                         ],
                         colors_dict={
                             'aro_ex_fd': [183, 183, 183],
                             'llanta_fd': [36, 36, 36],
                             'aro_in_fd': [159, 159, 159],
                             'aro_ex_td': [183, 183, 183],
                             'llanta_td': [36, 36, 36],
                             'aro_in_td': [159, 159, 159],
                             'aro_ex_ti': [183, 183, 183],
                             'llanta_ti': [36, 36, 36],
                             'aro_in_ti': [159, 159, 159],
                             'aro_ex_di': [183, 183, 183],
                             'llanta_di': [36, 36, 36],
                             'aro_in_di': [159, 159, 159],
                             'chasis': [0, 255, 221],
                             'resto': [51, 51, 51]
                         })

    grass_positions_x = [-3.79, -1.07 - 2, 2.1, 1.31 + 1, 2.03 - 1, 0.28 + 3, -0.31 - 2, -0.84 - 3, 0.48, -2.13 - 1,
                         -2.91, -0.71 - 4,
                         -3.93, 1.22, -0.57 + 3, 1.85, -0.66 - 3, -3.0, -0.96 + 3, -0.61 + 3, 2.21, -3.55, -4.75, 1.33,
                         -1.69 - 2, -3.58, 1.06, 0.61, 1.8, -4.27]
    grass_positions_z = [2.0, -5.03, 1.29, 3.34, -2.88, -5.49, -6.61, -5.28, -2.94, -0.33, -3.37, 2.39, -3.11, -1.83,
                         1.47, 3.75, -6.07, -2.85, 0.87, -0.34, -3.74, -0.04, 1.19, 1.57, -0.39, 3.0, -3.29, -6.0,
                         -6.79, 1.55]

    simple_tree = ObjectFromFile('Tree/lowpolytree.obj',
                                 scale_factor=0.5,
                                 colors_dict={'leaves': [78, 140, 31],
                                              'bark': [102, 59, 25]})

    street = Street()
    wolf = ObjectFromFile('nfts8bvk6s5c-Wolf/Wolf.obj',
                          scale_factor=0.003,
                          colors_dict={'wolf': [135, 142, 150]})

    rocks = ObjectFromFile('BlenderNatureAsset/BlenderNatureAsset.obj',
                           materials_list=[
                               'Material.004_17', 'Material.005_25', 'Material.006_26'
                           ],
                           scale_factor=0.4,
                           colors_dict={
                               'rock_1': [71, 81, 83],
                               'rock_2': [71, 81, 83],
                               'rock_3': [71, 81, 83]
                           })

    grass_color = [34, 136, 62]
    grass = ObjectFromFile('BlenderNatureAsset/BlenderNatureAsset.obj',
                           materials_list=[
                               'Material.007_0', 'Material.007_1', 'Material.007_2', 'Material.007_3', 'Material.007_4'
                           ],
                           scale_factor=0.4,
                           colors_dict={
                               'grass_1': grass_color,
                               'grass_2': grass_color,
                               'grass_3': grass_color,
                               'grass_4': grass_color,
                               'grass_5': grass_color
                           })

    special_tree = ObjectFromFile('BlenderNatureAsset/BlenderNatureAsset.obj',
                                  materials_list=[
                                      'troc_21', 'Material.001_22', 'Material.002_23', 'Material.003_24'
                                  ],
                                  scale_factor=0.7,
                                  colors_dict={
                                      'bark': [102, 59, 25],
                                      'leaves_1': [78, 140, 31],
                                      'leaves_2': [78, 140, 31],
                                      'leaves_3': [78, 140, 31],
                                  })

    house_1 = ObjectFromFile('12-asset-pack.obj/Asset Pack.obj',
                             scale_factor=1.2,
                             materials_list=[
                                 'Material.025_54', 'Material.026_55',
                                 'Material.014_56', 'Material.031_57', 'Material.030_58', 'Material.029_59',
                                 'Material_60', 'Material.019_61', 'Material.018_62', 'Material.017_63',
                                 'Material.013_64'
                             ],
                             colors_dict={
                                 'marco_ventana': [60, 41, 35],
                                 'vidrio_ventana': [133, 167, 195],
                                 'maderas': [127, 72, 41],
                                 'pomo': [60, 41, 35],
                                 'puerta': [59, 30, 26],
                                 'madera_central': [127, 72, 41],
                                 'techo': [127, 72, 41],
                                 '8': [250, 250, 250],
                                 '9': [209, 209, 209],
                                 '10': [150, 150, 150],
                                 '11': [250, 250, 250]
                             })

    glutInit()  # initialize glut
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH)

    glutInitWindowSize(width, height)
    glutInitWindowPosition(0, 0)
    window = glutCreateWindow("3D World")

    glutFullScreen()

    glutDisplayFunc(draw)
    glutIdleFunc(draw)

    glutEntryFunc(check_bound)
    glutWarpPointer(width // 2, height // 2)
    glutSetCursor(GLUT_CURSOR_NONE)

    glutPassiveMotionFunc(hold_movement)
    glutKeyboardFunc(key_function)

    glutMainLoop()

from ctypes import c_float, c_short, c_char
from OpenGL.GL import *
import pickle

from parser_patito import Parser

import numpy as np


class ParsedObject:
    def __init__(self):
        with open('vertex_array.pickle', 'rb') as f:
            vertex_array = pickle.load(f)

        vertex = vertex_array

        self.vertex_c = (c_float * len(vertex))(*vertex)

        with open('index_array_triangles_1.pickle', 'rb') as f:
            index_array_1 = pickle.load(f)
        indices = index_array_1
        self.indices_1_c = (c_short * len(indices))(*indices)

        with open('index_array_triangles_2.pickle', 'rb') as f:
            index_array_2 = pickle.load(f)
        indices = index_array_2
        self.indices_2_c = (c_short * len(indices))(*indices)

        self.n_dims = 3

    def draw(self):
        glEnableClientState(GL_VERTEX_ARRAY)

        glVertexPointer(self.n_dims, GL_FLOAT, 0, self.vertex_c)

        glColor(78/255, 140/255, 31/255)
        glDrawElements(GL_TRIANGLES, len(self.indices_1_c), GL_UNSIGNED_SHORT, self.indices_1_c)

        glColor(102/255, 59/255, 25/255)
        glDrawElements(GL_TRIANGLES, len(self.indices_2_c), GL_UNSIGNED_SHORT, self.indices_2_c)

        glDisableClientState(GL_VERTEX_ARRAY)


class ObjectFromFile:
    def __init__(self, file_path: str, scale_factor=1, colors_dict={}, materials_list=[]):
        parser = Parser(file_path)
        vertex_array, indices_dict = parser.parse(scale_factor)

        vertex = vertex_array

        self.vertex_c = (c_float * len(vertex))(*vertex)

        # self.indices_c = (c_short * len(indices))(*indices)
        self.indices_c = []
        for segment_name, index_array in indices_dict.items():
            if materials_list and segment_name not in materials_list:
                continue
            self.indices_c.append((c_short * len(index_array))(*index_array))

        self.n_dims = 3

        self.material_names = list(indices_dict.keys())

        self.colors = np.asarray(list(colors_dict.values()))/255

    def draw(self):
        glEnableClientState(GL_VERTEX_ARRAY)

        glVertexPointer(self.n_dims, GL_FLOAT, 0, self.vertex_c)

        # glColor(78 / 255, 140 / 255, 31 / 255)
        for i, index_array in enumerate(self.indices_c):
            try:
                color = self.colors[i]
            except IndexError:
                color = None

            if not isinstance(color, type(None)):
                glColor(*color)
            glDrawElements(GL_TRIANGLES, len(index_array), GL_UNSIGNED_SHORT, index_array)

        glDisableClientState(GL_VERTEX_ARRAY)


class Street:
    def __init__(self):

        vertex = [
            # Street top
            -.4, -1, 0,  # 0
            0.4, -1, 0,  # 1
            0.4, .9, 0,  # 2
            -.4, .9, 0,  # 3
            # Street bottom
            -.4, -1, -.1,  # 4
            0.4, -1, -.1,  # 5
            0.4, .9, -.1,  # 6
            -.4, .9, -.1,  # 7
            # Line 1
            -.05, -.8, 0.001,  # 8
            0.05, -.8, 0.001,  # 9
            0.05, -.6, 0.001,  # 10
            -.05, -.6, 0.001,  # 11
            # Line 2
            -.05, -.3, 0.001,  # 12
            0.05, -.3, 0.001,  # 13
            0.05, -.1, 0.001,  # 14
            -.05, -.1, 0.001,  # 15
            # Line 3
            -.05, 0.2, 0.001,  # 16
            0.05, 0.2, 0.001,  # 17
            0.05, 0.4, 0.001,  # 18
            -.05, 0.4, 0.001,  # 19
            # Line 4
            -.05, 0.7, 0.001,  # 20
            0.05, 0.7, 0.001,  # 21
            0.05, 0.9, 0.001,  # 22
            -.05, 0.9, 0.001,  # 23
            # Grass left top
            -1.5, -1, 0,  # 24
            -0.4, -1, 0,  # 25
            -0.4, .9, 0,  # 26
            -1.5, .9, 0,  # 27
            # Grass left bottom
            -1.5, -1, -.1,  # 28
            -0.4, -1, -.1,  # 29
            -0.4, .9, -.1,  # 30
            -1.5, .9, -.1,  # 31
            # Grass right top
            0.4, -1, 0,  # 32
            1.5, -1, 0,  # 33
            1.5, .9, 0,  # 34
            0.4, .9, 0,  # 35
            # Grass right bottom
            0.4, -1, -.1,  # 36
            1.5, -1, -.1,  # 37
            1.5, .9, -.1,  # 38
            0.4, .9, -.1,  # 39
        ]
        vertex = np.asarray(vertex) * 3

        self.vertex_c = (c_float * len(vertex))(*vertex)

        indices_dict = {
            'street': [
                # Top
                0, 1, 2,
                0, 2, 3,
                # Bottom
                4, 5, 6,
                4, 6, 7,
                # Front
                0, 1, 5,
                0, 5, 4,
                # Back
                3, 2, 6,
                3, 6, 7
            ],
            'line_1': [
                8, 9, 10,
                8, 10, 11
            ],
            'line_2': [
                12, 13, 14,
                12, 14, 15
            ],
            'line_3': [
                16, 17, 18,
                16, 18, 19
            ],
            'line_4': [
                20, 21, 22,
                20, 22, 23
            ],
            'grass_left': [
                # top
                24, 25, 26,
                24, 26, 27,
                # bottom
                28, 29, 30,
                28, 30, 31,
                # front
                24, 25, 29,
                24, 29, 28,
                # back
                27, 26, 30,
                27, 30, 31,
                # left
                31, 27, 24,
                31, 24, 28
            ],
            'grass_right': [
                # top
                32, 33, 34,
                32, 34, 35,
                # bottom
                36, 37, 38,
                36, 38, 39,
                # front
                32, 33, 37,
                32, 37, 36,
                # back
                35, 34, 38,
                35, 38, 39,
                # right
                34, 38, 37,
                34, 37, 33
            ],
        }

        self.colors_dict = {
            'street': np.asarray([50, 52, 59]),
            'line_1': np.asarray([187, 154, 85]),
            'line_2': np.asarray([187, 154, 85]),
            'line_3': np.asarray([187, 154, 85]),
            'line_4': np.asarray([187, 154, 85]),
            'grass_left': np.asarray([78, 120, 31]),
            'grass_right': np.asarray([78, 120, 31]),
        }

        self.indices_c = []
        for segment_name, index_array in indices_dict.items():
            self.indices_c.append((c_short * len(index_array))(*index_array))

        self.n_dims = 3

        self.material_names = list(indices_dict.keys())

    def draw(self):
        glEnableClientState(GL_VERTEX_ARRAY)

        glVertexPointer(self.n_dims, GL_FLOAT, 0, self.vertex_c)

        glPushMatrix()
        glRotate(270, 1, 0, 0)
        for index_array, color in zip(self.indices_c, self.colors_dict.values()):
            glColor(*(color/255))
            glDrawElements(GL_TRIANGLES, len(index_array), GL_UNSIGNED_SHORT, index_array)
        glPopMatrix()

        glDisableClientState(GL_VERTEX_ARRAY)
